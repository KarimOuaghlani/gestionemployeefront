import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-personne-info',
  templateUrl: './personne-info.component.html',
  styleUrls: ['./personne-info.component.scss']
})
export class PersonneInfoComponent {
  @Input() data!: any 
  //@Output() item = new EventEmitter()
}

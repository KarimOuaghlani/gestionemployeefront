import { Component } from '@angular/core';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-employee-by-company',
  templateUrl: './employee-by-company.component.html',
  styleUrls: ['./employee-by-company.component.scss']
})
export class EmployeeByCompanyComponent {

  constructor(private personneService: PersonneService) {}

  searchCompany: string = '';
  employeesByCompany: any;

  searchEmployeesByCompany() {
    this.personneService.getPersonneByCompanyName(this.searchCompany).subscribe(data=>{
      this.employeesByCompany =  data;
    })

    
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeByCompanyComponent } from './employee-by-company.component';

describe('EmployeeByCompanyComponent', () => {
  let component: EmployeeByCompanyComponent;
  let fixture: ComponentFixture<EmployeeByCompanyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeeByCompanyComponent]
    });
    fixture = TestBed.createComponent(EmployeeByCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../services/personne.service';
import { EmploiService } from '../services/emploi.service';

@Component({
  selector: 'app-emploi',
  templateUrl: './emploi.component.html',
  styleUrls: ['./emploi.component.scss']
})
export class EmploiComponent implements OnInit{

  personnes: any;
  selectedItem: any ={};
  emploi: any = {}; 
  emplois: any[] = []; 
  success: boolean = false;
  failed: boolean = false;
  message: any;

  constructor(private personneService: PersonneService, private emploiService: EmploiService) { }

  ngOnInit(): void {
   this.getAllPersonne();
  }
  
  getAllPersonne(){
    this.personneService.getAllEmployee().subscribe(data=>{
      this.personnes = data;
    })
  }

  addEmploi() {
    this.emplois.push({ ...this.emploi });
    this.emploi = {};
  }

  onSelectChange(event: any) {
    this.selectedItem = event.target.value;
  }

  createEmploi() {
    this.emploiService.createJob(this.emplois, this.selectedItem).subscribe(data=>{
    this.emplois = [];
    this.message = data;
    if(this.message.statusCode == 'BAD_REQUEST'){
      this.failed = true;
    }else{
      this.success = true;
    }
    console.log(data)
    }, error =>{
      alert(error.message);
    });
    
  }
}

import { Component } from '@angular/core';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.scss']
})
export class PersonneComponent {

  nouvellePersonne: any = {};
  success: boolean = false;
  isAgeInvalid: boolean = false;
  constructor(private personneService: PersonneService) {}

  createPersonne() {
    const birthDate = new Date(this.nouvellePersonne.dateDeNaissance);
    const age = new Date().getFullYear() - birthDate.getFullYear();
    if (age > 150) {
      this.isAgeInvalid = true;
    } else {
      this.isAgeInvalid = false;
      this.personneService.createPersonne(this.nouvellePersonne)
      .subscribe((data) => {
        this.success = true;
      }, error =>{
        alert(error.message);
      });
    }
   
  }

}

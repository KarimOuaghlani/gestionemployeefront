import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmploiService {

  constructor(private http: HttpClient) { }

  createJob(newJob: any, idPersonne: any) {
    return this.http.post(environment.baseApi + '/emplois/'+ idPersonne, newJob);
  }

  

  }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  constructor(private http: HttpClient) { }

  createPersonne(nouvellePersonne: any) {
    return this.http.post(environment.baseApi + '/personnes', nouvellePersonne);
  }

  getAllEmployee() {
    return this.http.get(environment.baseApi + '/personnes/all');
  }
  
  getAllSortedEmployeesByAlphabeticalOrder() {
    return this.http.get(environment.baseApi + '/personnes/sorted');
  }

  getPersonneByCompanyName(companyName: any) {
    return this.http.get(environment.baseApi + '/personnes/' + companyName);
  }

  JobByDateRange(dateDebut: any, dateFin: any, idPersonne:any) {
    return this.http.get(environment.baseApi + '/personnes/between?dateDebut='+dateDebut+'&dateFin='+dateFin+'&idPersonne='+idPersonne);
  }

}

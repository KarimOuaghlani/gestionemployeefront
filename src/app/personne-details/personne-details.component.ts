import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personne-details',
  templateUrl: './personne-details.component.html',
  styleUrls: ['./personne-details.component.scss']
})
export class PersonneDetailsComponent implements OnInit{
  nom:any;
  prenom:any;
  data!:any;
  employees: any;

  constructor(private route:ActivatedRoute) {
    this.nom = this.route.snapshot.paramMap.get("nom");
    this.prenom = this.route.snapshot.paramMap.get("prenom");
   }
   
  ngOnInit(): void {
    const storedData = localStorage.getItem('listPersonnes');
    this.employees = JSON.parse(storedData!);
    const employee = this.employees.find((e: { nom: any; prenom: any}) => e.nom === this.nom && e.prenom === this.prenom);
    this.data = employee;

  }
}

import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-list-personne',
  templateUrl: './list-personne.component.html',
  styleUrls: ['./list-personne.component.scss']
})
export class ListPersonneComponent implements OnInit {
  spinnerloading:boolean = false;
  personnes: any;
  
  constructor(private personneService: PersonneService) {}

  ngOnInit(): void {
    this.spinnerloading = true;
    this.personneService.getAllSortedEmployeesByAlphabeticalOrder().subscribe(data=>{
      this.personnes = data;
      localStorage.setItem("listPersonnes", JSON.stringify(data));
      this.spinnerloading = false;
    })
    
  }
}

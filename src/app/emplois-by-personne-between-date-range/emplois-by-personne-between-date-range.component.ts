import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-emplois-by-personne-between-date-range',
  templateUrl: './emplois-by-personne-between-date-range.component.html',
  styleUrls: ['./emplois-by-personne-between-date-range.component.scss']
})
export class EmploisByPersonneBetweenDateRangeComponent implements OnInit{ 
  
  startDate: string = '';
  endDate: string = '';
  data!: any;
  selectedItem: any ={};
  personnes: any;

  constructor(private personneService: PersonneService) { }
  
  ngOnInit(): void {
    this.getAllPersonne();
   }
   
   getAllPersonne(){
     this.personneService.getAllEmployee().subscribe(data=>{
       this.personnes = data;
     })
   }

  onSelectChange(event: any) {
    this.selectedItem = event.target.value;
  }

  searchJobsByDateRange() {
    this.personneService.JobByDateRange(this.startDate, this.endDate, this.selectedItem).subscribe(res=>{
        this.data = res;
     })
  }
}

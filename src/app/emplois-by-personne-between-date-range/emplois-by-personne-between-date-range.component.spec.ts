import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploisByPersonneBetweenDateRangeComponent } from './emplois-by-personne-between-date-range.component';

describe('EmploisByPersonneBetweenDateRangeComponent', () => {
  let component: EmploisByPersonneBetweenDateRangeComponent;
  let fixture: ComponentFixture<EmploisByPersonneBetweenDateRangeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmploisByPersonneBetweenDateRangeComponent]
    });
    fixture = TestBed.createComponent(EmploisByPersonneBetweenDateRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

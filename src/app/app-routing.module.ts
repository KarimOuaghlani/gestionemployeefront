import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { PersonneComponent } from './personne/personne.component';
import { EmploiComponent } from './emploi/emploi.component';
import { ListPersonneComponent } from './list-personne/list-personne.component';
import { PersonneDetailsComponent } from './personne-details/personne-details.component';
import { EmployeeByCompanyComponent } from './employee-by-company/employee-by-company.component';
import { EmploisByPersonneBetweenDateRangeComponent } from './emplois-by-personne-between-date-range/emplois-by-personne-between-date-range.component';


const routes: Routes = [
  { path: 'personne', component: PersonneComponent },
  { path: 'listPersonne', component: ListPersonneComponent },
  { path: 'emploi', component: EmploiComponent },
  { path:"details/:nom/:prenom", component:PersonneDetailsComponent},
  { path:'employeeByCompany', component:EmployeeByCompanyComponent},
  { path:'JobByDateRange', component:EmploisByPersonneBetweenDateRangeComponent},
  { path: '', redirectTo: '/personne', pathMatch: 'full' },
  { path: '**', redirectTo: '/personne' },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

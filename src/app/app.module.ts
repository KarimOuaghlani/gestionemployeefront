import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonneComponent } from './personne/personne.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmploiComponent } from './emploi/emploi.component';
import { HeaderComponent } from './header/header.component';
import { ListPersonneComponent } from './list-personne/list-personne.component';
import { PersonneInfoComponent } from './personne-info/personne-info.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { PersonneDetailsComponent } from './personne-details/personne-details.component';
import { EmployeeByCompanyComponent } from './employee-by-company/employee-by-company.component';
import { EmploisByPersonneBetweenDateRangeComponent } from './emplois-by-personne-between-date-range/emplois-by-personne-between-date-range.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonneComponent,
    EmploiComponent,
    HeaderComponent,
    ListPersonneComponent,
    PersonneInfoComponent,
    SpinnerComponent,
    PersonneDetailsComponent,
    EmployeeByCompanyComponent,
    EmploisByPersonneBetweenDateRangeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
